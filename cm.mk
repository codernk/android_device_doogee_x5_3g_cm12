# Release name
PRODUCT_RELEASE_NAME := x5_3g

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/doogee/x5_3g/device_x5_3g.mk)

# Correct bootanimation size for the screen
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

## Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_x5_3g
PRODUCT_DEVICE := x5_3g
PRODUCT_BRAND := doogee
PRODUCT_MANUFACTURER := DOOGEE
PRODUCT_MODEL := X5